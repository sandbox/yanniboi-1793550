api = 2
core = 7.23

projects[views] = 3.7
projects[ctools] = 1.3
projects[devel] = 1.3
projects[date] = 2.6
projects[search_api] = 1.8
projects[search_api_db] = 1.0-rc2
projects[email] = 1.2
projects[email_confirm] = 1.1
projects[entity] = 1.2
projects[entityreference] = 1.0
projects[name] = 1.9
projects[panels] = 3.3
projects[party] = 1.x-dev
projects[profile2] = 1.3
projects[simplenews] = 1.0
projects[views_bulk_operations] = 3.1

projects[party_extras][type] = "module"
projects[party_extras][download][url] = http://git.drupal.org/project/party_extras.git
projects[party_extras][download][revision] = 59eab06f26d71689be7bcb6a73168f89184f0e7e
projects[party_extras][download][branch] = 7.x-1.x
projects[party_extras][patch][2085487] = https://drupal.org/files/party_extras-Add_party_panels_tabs_module-2085487-1.patch
