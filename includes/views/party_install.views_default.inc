<?php

/**
 * Implements hook_views_default_views().
 */

function party_install_views_default_views() {
  $export = array();

  // Scan the 'views_default' directory for any .view files
  $files = file_scan_directory(dirname(__FILE__) . '/views_default', '/\.view$/', array('key' => 'name'));
  foreach ($files as $file) {
    if ((include $file->uri) == 1) {
      $export[$view->name] = $view;
    }
  }
  return $export;
}

