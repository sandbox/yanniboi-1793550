<?php

$plugin = array(
  'title' => t('Standard CNS Admin (with sidebar)'),
  'category' => t('CNS Admin'),
  'icon' => 'sidebar.png',
  'theme' => 'panels_sidebar',
  'css' => '../panels.css',
  'admin css' => '../panels-admin.css',
  'regions' => array(
    'sidebar' => t('Sidebar First'),
    'content' => t('Content'),
  ),
);
