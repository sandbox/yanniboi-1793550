<div class="panel-display panel-display-cns-admin flex-fill flex-container flex-horizontal">
  <?php if (!empty($content['sidebar'])) : ?>
  <div class="panel-sidebar sidebar-first flex-container">
    <?php print $content['sidebar']; ?>
  </div>
  <?php endif; ?>

  <div class="panel-content flex-fill flex-container">
    <?php print $content['content']; ?>
  </div>
</div>
