<?php
/**
 * @file
 */

/**
 * Implements hook_preprocess_html().
 */
function cns_admin_preprocess_html(&$vars) {
  // Add our flex class to body.
  $vars['classes_array'][] = 'flex-container';
}

/**
 * Implements hook_preprocess_block().
 */
function cns_admin_preprocess_block(&$vars) {
  // Add our flex classes to system main.
  if ($vars['block']->module == 'system' && $vars['block']->delta == 'main') {
    $vars['classes_array'][] = 'flex-fill';
    $vars['classes_array'][] = 'flex-container';
  }
}

/**
 * Implements hook_ctools_render_alter().
 */
function cns_admin_ctools_render_alter(&$info, &$page, &$context) {
/*  dpm($info, 'info');
  dpm($page, 'page');
  dpm($context, 'context');
  dpm($context['handler'], 'context->handler');
  if ($context['handler']->conf['display']->layout == 'sidebar') {

  }*/
}













/**
 * Add our flex classes to region content.
 */
/*function cns_admin_preprocess_region(&$vars) {
  if ($vars['region'] == 'content') {
    $vars['classes_array'][] = 'flex-fill';
    $vars['classes_array'][] = 'flex-container';
  }
}*/

/**
 * Add our flex classes to block system main.
 */
/*function cns_admin_preprocess_block(&$vars) {
  if ($vars['block']->module == 'system' && $vars['block']->delta == 'main') {
    $vars['classes_array'][] = 'flex-fill';
    $vars['classes_array'][] = 'flex-content';
    $vars['classes_array'][] = 'flex-container-content';
  }
}*/
