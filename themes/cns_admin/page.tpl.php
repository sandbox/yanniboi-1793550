
  <div id="branding" class="flex-container flex-row clearfix">
    <div class="logo">
      <?php if ($logo): ?>
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
        </a>
      <?php endif; ?>
    </div>
    <div class="branding-main flex-fill">
      <?php print $breadcrumb; ?>
      <?php print render($title_prefix); ?>
      <?php if ($title): ?>
        <h1 class="page-title"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php print render($primary_local_tasks); ?>
    </div>
  </div>

  <div id="page" class="flex-fill flex-container">
    <?php print render($page['content']); ?>
  </div>
