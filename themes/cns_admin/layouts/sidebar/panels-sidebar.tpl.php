<div class="panel-display panel-sidebar flex-fill flex-container flex-row">
  <div class="content flex-fill flex-container">
    <?php print $content['content']; ?>
  </div>

  <?php if (!empty($content['sidebar'])) : ?>
  <div class="sidebar sidebar-first flex-container">
    <?php print $content['sidebar']; ?>
  </div>
  <?php endif; ?>

  <?php if (!empty($content['sidebar_second'])) : ?>
  <div class="sidebar sidebar-second flex-container">
    <?php print $content['sidebar_second']; ?>
  </div>
  <?php endif; ?>
</div>
